package com.example.ex1_week1;

import com.example.ex1_week1.Model.Employee;
import com.example.ex1_week1.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ex1Week1Application implements CommandLineRunner {

    @Autowired
    private EmployeeRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(Ex1Week1Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Employee employee = new Employee(1L, "Quyet", "0971489598");
        repository.insert(employee);
//        repository.update("Long",employee.getId());
        /*
            Tạo id trùng để test transaction
         */
//        Employee employee1 = new Employee(1L, "Long","0355325647");
//        repository.insert(employee1);

    }
}
