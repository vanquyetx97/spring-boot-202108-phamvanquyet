package com.example.ex1_week1.Repository;


import com.example.ex1_week1.Model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional(rollbackOn = {Exception.class})
public class EmployeeRepository {

    @Autowired
    private EntityManager entityManager;

    // sử dụng native SQL
    public void insert(Employee employee) throws Exception {
        entityManager.createNativeQuery("INSERT INTO EMPLOYEE(id,name,phone) VALUES (?,?,?)")
                .setParameter(1,employee.getId())
                .setParameter(2,employee.getName())
                .setParameter(3,employee.getPhone())
                .executeUpdate();
        System.out.println("Insert dữ liệu thành công ! ");

//        sử dụng hàm exception để test transaction
//        demoException();
    }
    /*
        @Query sử dụng bên trong interface
     */
//    @Modifying
//    @Query("UPDATE EMPLOYEE set name=:name where id=:id")
//    public void update(@Param("name") String name,@Param("id") long id);

    public void demoException() throws Exception {
        throw new Exception(" Exception");
    }
}

