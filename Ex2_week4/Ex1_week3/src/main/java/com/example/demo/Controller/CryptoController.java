package com.example.demo.Controller;

import com.example.demo.Model.Request.CryptoRequest;
import com.example.demo.Model.Response.CryptoExchangeResponse;
import com.example.demo.Model.Response.CryptoResponse;
import com.example.demo.Model.Response.ResponseEntity;
import com.example.demo.Service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.util.List;

@Controller
@RequestMapping("crypto")
public class CryptoController {

    @Autowired
    private CryptoService cryptoService;

    @GetMapping
    public List<CryptoResponse> getListCrypto(@RequestParam(defaultValue = "0") int pageIndex,
                                              @RequestParam(defaultValue = "5") int pageSize) {
        return cryptoService.findAllCrypto(pageIndex, pageSize);
    }

    @GetMapping({"/keyword"})
    @ResponseStatus(HttpStatus.OK)
    public List<CryptoResponse> getCryptoBySymbol(@RequestParam(value = "name", required = false) String name,
                                                  @RequestParam(value = "address", required = false) String address,
                                                  @RequestParam(value = "symbol", required = false) String symbol,
                                                  @RequestParam(defaultValue = "0") int pageIndex,
                                                  @RequestParam(defaultValue = "5") int pageSize) {
        return cryptoService.findByNameAndAddressAndSymbol(name, address, symbol, pageIndex, pageSize);
    }

    @GetMapping({"/{symbol}/current-price"})
    @ResponseStatus(HttpStatus.OK)
    public List<CryptoExchangeResponse> getCurrentPriceOfSymbol(@PathVariable String symbol) {
        return cryptoService.getCurrentPriceOfSymbol(symbol);
    }

    @GetMapping({"/price"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity getPriceOfSymbolAtTime(@RequestParam (value = "symbol")String symbol,
                                                 @RequestParam(value = "time_at")Long time){
        return cryptoService.getPriceOfSymbolAtTime(symbol, time);
    }


    @PostMapping
    public org.springframework.http.ResponseEntity createCrypto(@RequestBody CryptoRequest crypto) {
        return org.springframework.http.ResponseEntity.ok(cryptoService.createCrypto(crypto));
    }

    @PutMapping("{id}")
    public org.springframework.http.ResponseEntity createCrypto(@Min(0) @PathVariable(value = "id") long id,
                                                                @RequestBody CryptoRequest crypto) {
        return org.springframework.http.ResponseEntity.ok(cryptoService.updateCrypto(id, crypto));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCrypto(@PathVariable long id) {
        cryptoService.deleteById(id);
    }
}
