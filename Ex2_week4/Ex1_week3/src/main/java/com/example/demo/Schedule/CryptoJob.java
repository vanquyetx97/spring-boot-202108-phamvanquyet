package com.example.demo.Schedule;

import com.example.demo.Model.Entity.Crypto;
import com.example.demo.Model.Entity.CryptoRateHistory;
import com.example.demo.Model.Response.CryptoExchangeResponse;
import com.example.demo.Repository.CryptoRateHistoryRepository;
import com.example.demo.Repository.CryptoRepository;
import com.example.demo.Service.CryptoService;
import com.example.demo.Service.Mapper.CryptoMapper;
import com.example.demo.Utils.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Component
public class CryptoJob {

    private final CryptoRepository cryptoRepository;
    private final CryptoService cryptoServices;
    private final List<String> ListCoinGeckoId;
    private final String ListCoinGeckoParam;
    private final CryptoMapper cryptoMapper;
    private List<Crypto> cryptos;
    private final CryptoRateHistoryRepository cryptoRateHistoryRepository;

    public CryptoJob(CryptoRepository cryptoRepository,CryptoMapper cryptoMapper,
                     CryptoService cryptoServices,CryptoRateHistoryRepository cryptoRateHistoryRepository){
        this.cryptoRepository = cryptoRepository;
        this.cryptoMapper = cryptoMapper;
        this.cryptoServices = cryptoServices;
        this.ListCoinGeckoId = cryptoServices.getAllCoinGeckoId();
        this.ListCoinGeckoParam = StringUtils.convertListToString(ListCoinGeckoId);
        this.cryptoRateHistoryRepository = cryptoRateHistoryRepository;
    }

    /**
     *  Lập lịch chạy mỗi 1 phút
     *  Xoá những bản ghi quá hạn 60p
     *  cập nhật tỷ giá của các crypto
     */
    @Scheduled(cron = "0 * * * * *")
    public void getPriceForAllCryptoPerMinute(){
        cryptoRateHistoryRepository.deleteRateHistoryAfterTime(60);
        List<CryptoExchangeResponse> cryptoCurrentPriceList =
                cryptoServices.getCurrentPriceOfSymbols(ListCoinGeckoParam);
        cryptos = cryptoRepository.findAll();
        for(Crypto c : cryptos){
            for(CryptoExchangeResponse cryptoExchangeDTO : cryptoCurrentPriceList){
                if(c.getSymbol().equalsIgnoreCase(cryptoExchangeDTO.getSymbol())){
                    CryptoRateHistory cryptoRateHistory = new CryptoRateHistory(cryptoExchangeDTO.getCurrentPrice(),
                            new Timestamp(System.currentTimeMillis()),c);
                    cryptoRateHistoryRepository.saveAndFlush(cryptoRateHistory);
                    cryptoCurrentPriceList.remove(cryptoExchangeDTO);
                    break;
                }
            }
        }
    }
}
