package com.example.demo.Model.Entity;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "crypto_rate_history")
public class CryptoRateHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "current_price")
    private BigDecimal current_price;

    @Column(name = "create_at")
    @CreatedDate
    private Timestamp create_at;

    @ManyToOne
    @JoinColumn(name = "crypto_id")
    private Crypto crypto;

    public CryptoRateHistory() {
        //Init constructor
    }

    public CryptoRateHistory(BigDecimal current_price, Timestamp create_at, Crypto crypto) {
        this.current_price = current_price;
        this.create_at = create_at;
        this.crypto = crypto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCurrent_price() {
        return current_price;
    }

    public void setCurrent_price(BigDecimal current_price) {
        this.current_price = current_price;
    }

    public Timestamp getCreate_at() {
        return create_at;
    }

    public void setCreate_at(Timestamp create_at) {
        this.create_at = create_at;
    }

    public Crypto getCrypto() {
        return crypto;
    }

    public void setCrypto(Crypto crypto) {
        this.crypto = crypto;
    }
}
