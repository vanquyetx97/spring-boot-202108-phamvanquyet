package com.example.demo.Model.Response;

import java.math.BigDecimal;

public class CryptoExchangeResponse {

    private String symbol;
    private String name;
    private BigDecimal currentPrice;

    public CryptoExchangeResponse() {
        //Init constructor
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }
}
