package com.edso.ex1_week4.Consumer.Model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.ToString;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id",scope = com.edso.ex1_week4.Consumer.Model.Employee.class)
@Data
@ToString
public class Employee {

    private String empName;
    private String empId;
    private int salary;

}