package com.example.ex1_week2.Repository;

import com.example.ex1_week2.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
