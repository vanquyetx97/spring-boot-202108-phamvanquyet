package com.example.ex1_week2.Service;

import com.example.ex1_week2.Model.Employee;
import com.example.ex1_week2.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;


    public List<Employee> findAll() {
        return repository.findAll();
    }


    public Employee getById(Long id) {
        return repository.getById(id);
    }

    public Employee update(Long id, Employee employee) {
        Optional<Employee> employeeData = repository.findById(id);
        if (employeeData.isPresent()) {
            Employee emp = employeeData.get();
            emp.setName(employee.getName());
            return repository.save(employee);
        } else {
            employee.setId(id);
            return repository.save(employee);
        }
    }

    public void save(Employee emp) {
        repository.save(emp);
    }


    public void delete(Long id) {
        repository.deleteById(id);
    }

}
