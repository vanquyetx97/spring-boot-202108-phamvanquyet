package com.example.ex1_week2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ex1Week2Application {

    public static void main(String[] args) {
        SpringApplication.run(Ex1Week2Application.class, args);
    }

}
