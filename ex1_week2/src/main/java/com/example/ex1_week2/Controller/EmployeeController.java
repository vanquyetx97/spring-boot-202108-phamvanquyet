package com.example.ex1_week2.Controller;


import com.example.ex1_week2.Model.Employee;
import com.example.ex1_week2.Service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.NoSuchElementException;


@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employee")
    public List<Employee> index() {
        return employeeService.findAll();
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getById(@PathVariable Long id) {
        try {
            Employee employee = employeeService.getById(id);
            return new ResponseEntity<Employee>(employee, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/employee/create")
    public void create(Employee employee) {
        employeeService.save(employee);
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<?> updateEmployee
            (@Min(0) @PathVariable("id") Long id,
             @Valid @RequestBody Employee employee) {
        try {
            Employee exitEmployee = employeeService.getById(id);
            employeeService.save(employee);
            return new ResponseEntity<Employee>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<Employee>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/employee/{id}")
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
    }

}
