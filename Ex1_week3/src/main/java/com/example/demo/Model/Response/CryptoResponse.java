package com.example.demo.Model.Response;

import java.time.OffsetDateTime;

public class CryptoResponse {

    private String symbol;
    private String address;
    private OffsetDateTime createdAt;
    private OffsetDateTime updatedAt;
    private String isDeleted;
    private boolean whitelistCollateral;
    private boolean whitelistSupply;
    private String name;
    private String coinGeckoId;

    public CryptoResponse() {
        //Init Constructor
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public boolean isWhitelistCollateral() {
        return whitelistCollateral;
    }

    public void setWhitelistCollateral(boolean whitelistCollateral) {
        this.whitelistCollateral = whitelistCollateral;
    }

    public boolean isWhitelistSupply() {
        return whitelistSupply;
    }

    public void setWhitelistSupply(boolean whitelistSupply) {
        this.whitelistSupply = whitelistSupply;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoinGeckoId() {
        return coinGeckoId;
    }

    public void setCoinGeckoId(String coinGeckoId) {
        this.coinGeckoId = coinGeckoId;
    }
}
