package com.example.demo.Model.Entity;

import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "crypto")
public class Crypto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "int default 0")
    private long id;

    @Column(columnDefinition = "VARCHAR(64)")
    private String symbol;

    @Column(columnDefinition = "VARCHAR(64)")
    private String address;

    @CreatedDate
    @Column(columnDefinition = "timestamp default now()")
    private OffsetDateTime createdAt;

    @LastModifiedDate
    @UpdateTimestamp
    @Column(columnDefinition = "timestamp")
    private OffsetDateTime updatedAt;

    @Column(columnDefinition = "VARCHAR(64)")
    private String isDeleted;

    @Column(columnDefinition = "boolean default true")
    private boolean whitelistCollateral;

    @Column(columnDefinition = "boolean default true")
    private boolean whitelistSupply;

    @Column(columnDefinition = "VARCHAR(64)")
    private String name;

    @Column(columnDefinition = "VARCHAR(64)")
    private String coinGeckoId;

    public Crypto() {
        //Init Constructor
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime created_at) {
        this.createdAt = createdAt;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(OffsetDateTime updated_at) {
        this.updatedAt = updatedAt;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public boolean isWhitelistCollateral() {
        return whitelistCollateral;
    }

    public void setWhitelistCollateral(boolean whitelistCollateral) {
        this.whitelistCollateral = whitelistCollateral;
    }

    public boolean isWhitelistSupply() {
        return whitelistSupply;
    }

    public void setWhitelistSupply(boolean whitelistSupply) {
        this.whitelistSupply = whitelistSupply;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoinGeckoId() {
        return coinGeckoId;
    }

    public void setCoinGeckoId(String coinGeckoId) {
        this.coinGeckoId = coinGeckoId;
    }

    @PrePersist
    void onCreate(){
        this.setCreatedAt(OffsetDateTime.now());
    }

    @PreUpdate
    void onPersist(){
        this.setUpdatedAt(OffsetDateTime.now());
    }
}
