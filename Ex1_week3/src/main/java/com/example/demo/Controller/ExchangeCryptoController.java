package com.example.demo.Controller;

import com.example.demo.Service.CryptoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class ExchangeCryptoController {

    @Autowired
    private CryptoService cryptoService;

    @GetMapping({"/exchange"})
    @ResponseStatus(HttpStatus.OK)
    public BigDecimal exchangeCrypto(@RequestParam(value = "fromSymbol") String fromSymbol,
                                     @RequestParam(value = "fromAmount") BigDecimal fromAmount,
                                     @RequestParam(value = "toSymbol") String toSymbol){
        return cryptoService.exchangeSymbolTo(fromSymbol, fromAmount, toSymbol);
    }

}
