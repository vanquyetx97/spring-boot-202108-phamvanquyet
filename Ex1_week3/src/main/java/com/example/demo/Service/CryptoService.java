package com.example.demo.Service;

import com.example.demo.Model.Request.CryptoRequest;
import com.example.demo.Model.Response.CryptoExchangeResponse;
import com.example.demo.Model.Response.CryptoResponse;
import com.example.demo.Model.Response.ResponseEntity;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public interface CryptoService {

    List<CryptoResponse> findAllCrypto(int pageIndex, int pageSize);

    boolean createCrypto(CryptoRequest cryptoRequest);

    boolean updateCrypto(long id, CryptoRequest cryptoRequest);

    void deleteById(long id);

    List<CryptoResponse> findByNameAndAddressAndSymbol(String name, String address, String symbol, int pageIndex, int pageSize);

    List<CryptoExchangeResponse> getCurrentPriceOfSymbol(String symbol);

    BigDecimal exchangeSymbolTo(String fromSymbol, BigDecimal fromAmount, String toSymbol);

    List<String> getAllCoinGeckoId();

    List<CryptoExchangeResponse> getCurrentPriceOfSymbols(String ids);

    ResponseEntity getPriceOfSymbolAtTime(String symbol, Long timeAt);

}
