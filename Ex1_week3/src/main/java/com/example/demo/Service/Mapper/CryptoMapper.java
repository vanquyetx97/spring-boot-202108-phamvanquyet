package com.example.demo.Service.Mapper;

import com.example.demo.Model.Entity.Crypto;
import com.example.demo.Model.Request.CryptoRequest;
import com.example.demo.Model.Response.CryptoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CryptoMapper {

    /**
     * map dữ liệu từ Entity sang Response
     *
     * @param crypto
     * @return
     */
    public CryptoResponse map(Crypto crypto) {
        CryptoResponse cryptoResponse = new CryptoResponse();
        cryptoResponse.setSymbol(crypto.getSymbol());
        cryptoResponse.setAddress(crypto.getAddress());
        cryptoResponse.setCreatedAt(crypto.getCreatedAt());
        cryptoResponse.setUpdatedAt(crypto.getUpdatedAt());
        cryptoResponse.setIsDeleted(crypto.getIsDeleted());
        cryptoResponse.setWhitelistCollateral(crypto.isWhitelistCollateral());
        cryptoResponse.setWhitelistSupply(crypto.isWhitelistSupply());
        cryptoResponse.setName(crypto.getName());
        cryptoResponse.setCoinGeckoId(crypto.getCoinGeckoId());

        return cryptoResponse;
    }

    /**
     * map dữ liệu từ Request sang Entity
     *
     * @param cryptoRequest
     * @return
     */
    public Crypto map(CryptoRequest cryptoRequest) {
        Crypto crypto = new Crypto();
        crypto.setSymbol(cryptoRequest.getSymbol());
        crypto.setAddress(cryptoRequest.getAddress());
        crypto.setIsDeleted(cryptoRequest.getIsDeleted());
        crypto.setWhitelistCollateral(cryptoRequest.isWhitelistCollateral());
        crypto.setWhitelistSupply(cryptoRequest.isWhitelistSupply());
        crypto.setName(cryptoRequest.getName());
        crypto.setCoinGeckoId(cryptoRequest.getCoinGeckoId());

        return crypto;
    }

    /**
     *
     * @param cryptos
     * @return
     */
    public List<CryptoResponse> map(List<Crypto> cryptos) {
        List<CryptoResponse> cryptoResponseList = new ArrayList<>();
        cryptos.forEach(crypto -> cryptoResponseList.add(map(crypto)));
        return cryptoResponseList;
    }
}
