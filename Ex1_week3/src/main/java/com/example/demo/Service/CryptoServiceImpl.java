package com.example.demo.Service;

import com.example.demo.Model.Entity.Crypto;
import com.example.demo.Model.Request.CryptoRequest;
import com.example.demo.Model.Response.CryptoExchangeResponse;
import com.example.demo.Model.Response.CryptoResponse;
import com.example.demo.Model.Response.ResponseEntity;
import com.example.demo.Repository.CryptoRateHistoryRepository;
import com.example.demo.Repository.CryptoRepository;
import com.example.demo.Service.Mapper.CryptoMapper;
import com.example.demo.Specification.CryptoSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CryptoServiceImpl implements CryptoService {

    @Autowired
    private CryptoMapper cryptoMapper;

    @Autowired
    private CryptoRepository cryptoRepository;

    @Autowired
    private CryptoRateHistoryRepository cryptoRateHistoryRepository;

    private WebClient webClient;

    private final String COIN_GECKO_URL = "https://api.coingecko.com";
    private final String COIN_GECKO_BY_IDS =
            "/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false&ids={listID}";


    /**
     * Lấy all dữ liệu
     *
     * @return
     */
    @Override
    public List<CryptoResponse> findAllCrypto(int pageIndex, int pageSize) {
        Pageable paging = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> cryptoPage = cryptoRepository.findAll(paging);
        List<Crypto> cryptoList = cryptoPage.getContent();
        return cryptoMapper.map(cryptoList);

    }

    /**
     * Tìm kiếm theo name, address, symbol
     *
     * @param name
     * @param address
     * @param symbol
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<CryptoResponse> findByNameAndAddressAndSymbol(String name,
                                                              String address,
                                                              String symbol,
                                                              int pageIndex,
                                                              int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> page = cryptoRepository.findAll(CryptoSpecification.fineCrypto(name, address, symbol), pageable);
        List<Crypto> cryptos = page.getContent();
        return cryptoMapper.map(cryptos);

    }

    /**
     * Lấy Current Price theo Symbol
     *
     * @param symbol
     * @return
     */
    @Override
    public List<CryptoExchangeResponse> getCurrentPriceOfSymbol(String symbol) {
        List<Crypto> cryptoFoundBySymbol = cryptoRepository.findBySymbol(symbol);
        String ids = cryptoFoundBySymbol.stream()
                .map(crypto -> String.valueOf(crypto.getCoinGeckoId()))
                .collect(Collectors.joining(","));
        webClient = WebClient.create(COIN_GECKO_URL);
        List<CryptoExchangeResponse> response = getCurrentPriceOfSymbols(ids);
        return response;

    }

    /**
     * chuyển đổi giữa 2 cryptocurrency
     *
     * @param fromSymbol
     * @param fromAmount
     * @param toSymbol
     * @return
     */
    @Override
    public BigDecimal exchangeSymbolTo(String fromSymbol, BigDecimal fromAmount, String toSymbol) {
        String fromSymbolGeckoId = cryptoRepository.findBySymbol(fromSymbol).get(0).getCoinGeckoId();
        String toSymbolGeckoId = cryptoRepository.findBySymbol(toSymbol).get(0).getCoinGeckoId();
        String ids = fromSymbolGeckoId + "," + toSymbolGeckoId;
        List<CryptoExchangeResponse> response = getCurrentPriceOfSymbol(ids);
        BigDecimal fromSymbolPrice = null, toSymbolPrice = null;
        for (CryptoExchangeResponse cryptoExchangeDTO : response) {
            if (cryptoExchangeDTO.getSymbol().equalsIgnoreCase(fromSymbol)) {
                fromSymbolPrice = cryptoExchangeDTO.getCurrentPrice();
            }
            if (cryptoExchangeDTO.getSymbol().equalsIgnoreCase(toSymbol)) {
                toSymbolPrice = cryptoExchangeDTO.getCurrentPrice();
            }
        }
        BigDecimal toAmount = fromAmount.multiply(fromSymbolPrice.divide(toSymbolPrice, 5, RoundingMode.CEILING));
        return toAmount;

    }

    @Override
    public List<String> getAllCoinGeckoId() {
        List<Crypto> cryptos = cryptoRepository.findAll();
        List<String> symbols = cryptos.stream().map(crypto -> crypto.getCoinGeckoId()).collect(Collectors.toList());
        return symbols;
    }

    @Override
    public ResponseEntity getPriceOfSymbolAtTime(String symbol, Long timeAt) {
        Long crypto_id = cryptoRepository.findBySymbol(symbol).get(0).getId();
        BigDecimal priceAtTime = cryptoRateHistoryRepository.getPriceAtTimeNearest(crypto_id,timeAt);
        return new ResponseEntity(200,"Price at: ",priceAtTime);
    }

    /**
     * Tạo Crypto dạng boolean, tạo mới được thì trả về true
     *
     * @param cryptoRequest
     * @return
     */
    @Override
    public boolean createCrypto(CryptoRequest cryptoRequest) {
        Crypto crypto = cryptoMapper.map(cryptoRequest);
        cryptoRepository.save(crypto);
        return true;
    }

    /**
     * Update Crypto PathVariable theo id
     *
     * @param id
     * @param cryptoRequest
     * @return
     */
    @Override
    public boolean updateCrypto(long id, CryptoRequest cryptoRequest) {
        Crypto crypto = cryptoRepository.getById(id);
        cryptoMapper.map(cryptoRequest);
        cryptoRepository.save(crypto);
        return true;
    }

    /**
     * Delete Crypto theo id
     *
     * @param id
     */
    @Override
    public void deleteById(long id) {
        cryptoRepository.deleteById(id);
    }

    /**
     * @param ids
     * @return
     */
    public List<CryptoExchangeResponse> getCurrentPriceOfSymbols(String ids) {
        List<CryptoExchangeResponse> response = webClient.get()
                .uri(COIN_GECKO_BY_IDS, ids)
                .retrieve()
                .bodyToFlux(CryptoExchangeResponse.class)
                .collectList()
                .block();
        return response;
    }


}
