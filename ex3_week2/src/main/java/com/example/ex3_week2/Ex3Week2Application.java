package com.example.ex3_week2;

import com.example.ex3_week2.Repository.EmployeeRepository;
import com.example.ex3_week2.Service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ex3Week2Application implements CommandLineRunner {

    @Autowired
    private EmployeeService employeeService;

    public static void main(String[] args) {
        SpringApplication.run(Ex3Week2Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        insertEmployee();
        employeeService.showAllEmployee();
        System.out.println("------Tìm kiếm theo ID");
        employeeService.getEmployeeById(5);
        System.out.println("------Cập nhật employee có id = 5 :    ------");
        employeeService.updateEmployee(5);
        System.out.println("------Xoá employee có id =4: ---------");
        employeeService.deleteEmployee(4);
        System.out.println("------ Sau khi xoá và cập nhật: --------------");
        employeeService.showAllEmployee();
    }

}
