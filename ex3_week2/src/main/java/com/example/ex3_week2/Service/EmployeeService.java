package com.example.ex3_week2.Service;

import com.example.ex3_week2.Entity.Employee;
import com.example.ex3_week2.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Hiển thị toàn bộ documents
     */
    public void showAllEmployee() {
        List<Employee> listEmployee = employeeRepository.findAll();
        listEmployee.forEach(System.out::println);
    }

    /**
     * Tìm kiếm theo id
     * @param id
     */
    public void getEmployeeById(long id){
        try {
            Employee employee = employeeRepository.findById(id).get();
            System.out.println(employee);
        }catch (Exception e){
            System.out.println("Không tồn tại employee với id này");
        }
    }

    /**
     * Thêm một document vào collection
     */
    public void insertEmployee() {
        for (long i = 1; i <= 10; i++){{
            Employee e = new Employee(i,"Employee"+ i);
            employeeRepository.save(e);
        }}
    }

    /**
     * Cập nhật Employee
     * @param id
     */
    public void updateEmployee(long id){
        try {
            Employee employee = employeeRepository.findById(id).get();
            employee.setName("Cao The Thang");
            employeeRepository.save(employee);
        } catch (Exception e) {
            System.out.println("Không tồn tại Id");
            e.printStackTrace();
        }
    }

    /**
     *  Xoá employee
     * @param id
     */
    public void deleteEmployee(long id){
        try {
            employeeRepository.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
