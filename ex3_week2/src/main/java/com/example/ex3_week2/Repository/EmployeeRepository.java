package com.example.ex3_week2.Repository;

import com.example.ex3_week2.Entity.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee,Long> {

    @Query("{ 'name': ?0}")
    List<Employee> findByName(String name);
}
