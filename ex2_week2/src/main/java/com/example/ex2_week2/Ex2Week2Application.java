package com.example.ex2_week2;

import com.example.ex2_week2.Entity.Person;
import com.example.ex2_week2.Repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Ex2Week2Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Ex2Week2Application.class, args);
    }

    @Autowired
    private PersonRepository personRepository;

    @Override
    public void run(String... args) {
        long startTimeInsertPerson = System.nanoTime();
        initDataForPerson();
        long endTimeInsertPerson = System.nanoTime();
        System.out.println("Elapsed Time insert person in nano seconds: " +
                (endTimeInsertPerson - startTimeInsertPerson));
        long startTimeFindPerson = System.nanoTime();
        findPerson();
        long endTimeFindPerson = System.nanoTime();
        System.out.println("Elapsed Time find person in nano seconds: " +
                (endTimeFindPerson - startTimeFindPerson));
    }

    private void findPerson() {
        Person person = personRepository.findByName("Person 9999");
        System.out.println(person.getFullName());
    }


    public void initDataForPerson() {
        List<Person> listPerson = new ArrayList<>();
        for (int i = 1; i <= 2000000; i++) {
            Person person = new Person("Person " + i);
            listPerson.add(person);
        }
        personRepository.saveAll(listPerson);

    }

}
