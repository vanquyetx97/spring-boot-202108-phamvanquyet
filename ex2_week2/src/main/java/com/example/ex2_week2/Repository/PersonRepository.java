package com.example.ex2_week2.Repository;

import com.example.ex2_week2.Entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<Person,Long> {

    @Query("SELECT u FROM Person u WHERE u.fullName = ?1")
    Person findByName(String name);
}

